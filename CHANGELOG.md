# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## Unreleased

### Added

- test: add support Debian 12

### Changed

- chore: replace apt_key to get_url
- test: use personal docker registry

### Removed

- test: remove support Debian 11

## v1.1.0 - 2021-08-24

### Added

- test: add support debian 11

### Changed

- test: replace kitchen to molecule
- chore: use FQCN for module name

### Removed

- test: remove support debian 9

## v1.0.0 - 2019-09-06

### Added

- install kibana package
- copy configuration
