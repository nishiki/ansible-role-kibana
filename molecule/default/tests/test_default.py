import time
import testinfra.utils.ansible_runner

def test_packages(host):
  package = host.package('kibana')
  assert package.is_installed

def test_config_file(host):
  path = host.file('/etc/kibana/kibana.yml')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'kibana'
  assert path.mode == 0o640

def test_service(host):
  service = host.service('kibana')
  assert service.is_running
  assert service.is_enabled

def test_socket(host):
  socket = host.socket('tcp://0.0.0.0:5601')
  assert socket.is_listening
